export interface DestructuredPromise<T, E = unknown> {
    readonly promise: Promise<T>

    readonly isPending: boolean
    readonly isFulfilled: boolean
    readonly isRejected: boolean
    readonly isSettled: boolean
    readonly state: 'pending' | 'fulfilled' | 'rejected'
    readonly result: PromiseSettledResult<T> | undefined

    resolve(result: T | PromiseLike<T>): void
    reject(reason: E): void
}

/**
 * Create a new promise, destructuring its `resolve` and `reject` functions.
 * The returned object also includes getters to inspect the state of the promise.
 */
export default function pDestruct<T, E = unknown>(): DestructuredPromise<T, E> {
    let resolve: ((result: T | PromiseLike<T>) => void) | undefined = undefined
    let reject: ((reason: E) => void) | undefined = undefined
    let result: PromiseSettledResult<T> | undefined = undefined

    const promise = new Promise<T>((res, rej) => {
        resolve = res
        reject = rej
    })

    if (!resolve || !reject) {
        throw new Error('Promise constructor was not called synchronously')
    }

    promise.then(
        (value) => {
            result = { status: 'fulfilled', value }
        },
        (reason) => {
            result = { status: 'rejected', reason }
        }
    )

    return {
        promise,
        resolve,
        reject,

        get isPending () {
            return result === undefined
        },

        get isFulfilled () {
            return result?.status === 'fulfilled'
        },

        get isRejected () {
            return result?.status === 'rejected'
        },

        get isSettled () {
            return result !== undefined
        },

        get state () {
            return result?.status ?? 'pending'
        },

        get result () {
            return result
        }
    }
}
